#include "Player.h"
#include "Reward.h"

void PLAYER::move()
{
	if (Keyboard::isKeyPressed(Keyboard::Left))
	{
		goLeft();
	}
	if (Keyboard::isKeyPressed(Keyboard::Right))
	{
		goRight();
	}
	if (Keyboard::isKeyPressed(Keyboard::Up))
	{
		goUp();
	}
	if (Keyboard::isKeyPressed(Keyboard::Space)||Keyboard::isKeyPressed(Keyboard::D))
	{
		dig();
	}
	if (Keyboard::isKeyPressed(Keyboard::G))
	{
		grow();
	}
}
void PLAYER::update(double time, vector<Person*> enemies, Clock *protectionTime, vector<Person*> coins)
{
	if (rect.left>WindowWidth/2 && rect.left<tileMap->getWidth()*fieldSize-WindowWidth/2)
	{
		this->tileMap->setOffsetX(rect.left - WindowWidth/2);//�������� ������ � ����������� ��������� � ����� �������� ��� � �������� ������
	}
	if (rect.top>tileMap->getHeight()*fieldSize-tileMap->getHeight()*fieldSize/2 &&rect.top<ground/2)
	{
		this->tileMap->setOffsetY(rect.top - tileMap->getHeight()*fieldSize/2);
	}
	rect.left +=dx*time;
	Collision(DirX);

	if(!onGround) dy=dy+0.0005*time;
	rect.top+=dy*time;
	onGround=false;
	Collision(DirY);
	if(rect.top>ground-rect.height*scaleValueForPerson)//����� ������ ���� ����� ����� ���������� �����
	{
		rect.top = ground-rect.height*scaleValueForPerson; 
		dy=0;
		onGround = true;
	}
	currentFrame += 0.005*time;
	if (currentFrame>numberOfAnimations)
	{
		currentFrame-=numberOfAnimations;
	}

	if(dx>0) personSprite.setTextureRect(IntRect(sizePerson.first*int(currentFrame),15,100,sizePerson.second));
	if(dx<0) personSprite.setTextureRect(IntRect(sizePerson.first*int(currentFrame)+100,15,-100,sizePerson.second));

	personSprite.setPosition(rect.left-this->tileMap->getOffsetX(),rect.top-this->tileMap->getOffsetY());
	dx =0;
	if (protectionTime->getElapsedTime().asSeconds()>2)
	{
		this->personSprite.setColor(spriteColor);
		//hasProtection=false;
	}

	//if (enemies.size()!=NULL && hasProtection!=true) //����������� � ��������
	{
		bool alreadyKillEnemy = false; //���� �� ��� ������ �������, ������ ���� ����������� �� ����� �����
		for (int i = 0; i < enemies.size(); i++)
		{
			if (getScaledRect().intersects(enemies[i]->getScaledRect()))
			{
				if(dy>0)
				{
					enemies[i]->decreaseHitPoint();
					dy = -0.4;
					alreadyKillEnemy = true;
				}
				if (dy<=0 && !alreadyKillEnemy)
				{
					sf::Time elapsed = protectionTime->getElapsedTime();
					if(elapsed.asSeconds() >2)
					{
						hitPoint--;
						cerr<< this->getNameOfPerson()<<" hit point = " << hitPoint<<endl;
						protectionTime->restart();
						this->personSprite.setColor(Color::Green);
						enemies[i]->move();//������ ����� � ������ �������
						dx=-0.1;//�� ���� ������� �� �������
						//hasProtection=true;
					}
				}
			}
		}
	}
	if (coins.size()!=NULL)//������� �������
	{
		for (int i = 0; i < coins.size(); i++)
		{
			if (getScaledRect().intersects(coins[i]->getScaledRect()))
			{
				coins[i]->decreaseHitPoint();
			}
		}
	}

}
void PLAYER::update(double time)
{

}
void PLAYER:: printHitPoint (std::vector<SpriteHandler*> hitPoints, sf::RenderWindow* window)
{
	for (int i = 0; i < getHitPoint(); i++)
	{
		//SpriteHandler s;
		hitPoints[i]->setPosition(WindowWidth - 100 - 80*i, 100);
		window->draw(hitPoints[i]->getSprite());
	}
}
PLAYER::PLAYER(String pictureName, TileMap* currentMap) : Person(pictureName, currentMap)
{
	scaleValue.first = scaleValueForPerson;//0.5;
	scaleValue.second = scaleValueForPerson;
	sizePerson.first = 110;
	sizePerson.second = 120;
	rect = FloatRect(0,15,sizePerson.first,sizePerson.second);
	personSprite.setTextureRect((IntRect)rect);
	personSprite.scale(scaleValue.first,scaleValue.second);
	pair<int,int> pos = tileMap->getRandomEmptyField();
	rect.left = pos.second*fieldSize;
	rect.top = pos.first*fieldSize;
	//rect.left = 2*fieldSize;//300;//��������� ����������
	//rect.top = 10*fieldSize;//600;
	hitPoint = 3;
	spriteColor = personSprite.getColor();
}
std::string PLAYER::getNameOfPerson()
{
	return "Player";
}

void PLAYER::CollisionPersonForX(int numOfField)
{
	if (dx>0) 
	{
		rect.left = numOfField*fieldSize - rect.width*scaleValue.first;
	}
	if (dx<0)
	{
		rect.left = numOfField*fieldSize + fieldSize;
	}
}
void PLAYER::dig()
{
	int i = (rect.top+rect.height*scaleValue.second)/fieldSize;
	for (int j = rect.left/fieldSize; (j < (rect.left+rect.width*scaleValue.first)/fieldSize) /*&& j < tileMap->getWidth())*/; j++)
	{
		if (i<ground/fieldSize)//����� ��� ������
		{
			tileMap->tileMap[i][j]=skyTile; // ����
		}
	}
}
void PLAYER::grow()
{
	int i = (rect.top+rect.height*scaleValue.second)/fieldSize;
	if ((rect.top+rect.height*scaleValue.second)/fieldSize != (int)(rect.top+rect.height*scaleValue.second)/fieldSize)//���� ����� �� ������, �� ������ ����� ��� ����� ����
	{
		i++;
	}

	for (int j = rect.left/fieldSize; (j < (rect.left+rect.width*scaleValue.first)/fieldSize) /*&& j < tileMap->getWidth())*/; j++)
	{
		if (i<ground/fieldSize)//����� ��� ������
		{
			tileMap->tileMap[i][j]=playerTile; // ����� �����
		}
	}
}

PLAYER::~PLAYER()
{
}