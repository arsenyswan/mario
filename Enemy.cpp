#include "Enemy.h"
#include "TileMap.h"

void Enemy ::decreaseHitPoint(){hitPoint--;}
Enemy::Enemy(String pictureName, TileMap* const currentMap) : Person(pictureName, currentMap)
{
	cerr <<"Enemy 2 arg constructor \n";
	auto pos = tileMap->getRandomEmptyField();
	initialize(pictureName,pos.second*fieldSize,pos.first*fieldSize);
}
Enemy::Enemy(String pictureName, TileMap* const currentMap, int positionX, int positionY) : Person(pictureName, currentMap)
{
	cerr<<"Enemy 4 arg constructor \n";
	initialize(pictureName,positionX,positionY);	
}
void Enemy::update(double time)
{
	rect.left +=dx*time;
	Collision(DirX);

	if(!onGround) dy=dy+0.0005*time;
	rect.top+=dy*time;
	onGround=false;
	Collision(DirY);
	if(rect.top>ground-rect.height*scaleValueForPerson)//����� ������ ���� ����� ����� ���������� �����
	{
		rect.top = ground-rect.height*scaleValueForPerson; 
		dy=0;
		onGround = true;
	}
	currentFrame += 0.005*time;//����� ����� �������� 
	if (currentFrame>numberOfAnimations)
	{
		currentFrame-=numberOfAnimations;
	}

	if(dx>0) personSprite.setTextureRect(IntRect(sizePerson.first*int(currentFrame)+9,30,sizePerson.first,sizePerson.second));
	if(dx<0) personSprite.setTextureRect(IntRect(sizePerson.first*int(currentFrame)+sizePerson.first+9,30,-sizePerson.first,sizePerson.second));

	personSprite.setPosition(rect.left-this->tileMap->getOffsetX(),rect.top-this->tileMap->getOffsetY());
}

void Enemy::initialize(String pictureName, int positionX, int positionY)
{
	numberOfAnimations = 4;
	scaleValue.first = scaleValueForPerson-0.1;//0.5;
	scaleValue.second = scaleValueForPerson-0.1;
	sizePerson.first = 180;
	sizePerson.second = 200;
	rect = FloatRect(9,30,sizePerson.first,sizePerson.second);
	personSprite.setTextureRect((IntRect)rect);
	personSprite.scale(scaleValue.first,scaleValue.second);
	rect.left = positionX;
	rect.top = positionY;
	hitPoint = 1;
	speedOfEnemy = 0.05;
	dx = (rand()%100)>50?speedOfEnemy:-speedOfEnemy;
	Enemy::numberOfEnemy++;
}

void Enemy::CollisionPersonForX(int numOfField)
{
	if (dx>0)
	{
		rect.left = numOfField*fieldSize - rect.width*scaleValue.first;
		dx = -speedOfEnemy;
		return;
	}
	if (dx<0)
	{
		rect.left = numOfField*fieldSize + fieldSize;
		dx = speedOfEnemy;
		return;
	}
}

std::string Enemy::getNameOfPerson()
{
	return "Enemy";
}
//Enemy::Enemy()
//{
//}
void Enemy::move()
{
	dx=-dx;
}
Enemy::~Enemy()
{
	Enemy::numberOfEnemy--;
}
