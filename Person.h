#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>

using namespace sf;
#ifndef _PERSON_
#define _PERSON_
#include "TileMap.h"
const int ground = fieldSize*22;//tileMap.Hight
const float scaleValueForPerson = 0.4;//���������������� ��������� �������� ����� 0.5

const int WindowWidth = 1200;//������ ������������ ���� ����
const int numberOfEnemies = 10;//���������� ��������, � �����

class Person
{
public:
	Person(String pictureName, TileMap* const currentMap); //����������� ���������, �������� ��� ������� ����� ��� �������� � ���
	//Person(){};
	virtual ~Person()=0;
	virtual void move()=0;
	Sprite getPersonSprite() {return personSprite;} 
	int getHitPoint();
	void decreaseHitPoint();
	static std::vector<Person*> printPersons (std::vector<Person*> coins, sf::RenderWindow* window, double time);//��������� ���� ��������� �������(������ � �������)
	virtual void update(double time)=0;//������� ������� ��� ������� �������������� �������
	FloatRect getScaledRect();//���������� ������ �������������� ������� � �������� ������������ �� ������
	//void setPersonSprite(int x,int y) {personSprite.setPosition(30,300);}
protected:
	int numberOfAnimations;//���������� �������� �� .png ��������
	enum dirrection	{ DirX,DirY };//������������ ��� ����������� ����������� ������������ � ������� Colision
	virtual void CollisionPersonForX(int numOfField)=0;//��������� ����������� �� ���������� X ���� ��� ������� �������
	void Collision(dirrection dir);//��������� �����������, ������ �������� CollisionPersonForX
	Texture personTecture;//�������� �������
	float dx,dy;//������������ ��� ����������� �� ����������� x � y
	bool onGround;//��������� �� ������ �� �����
	FloatRect rect;//������������� ��� ����������� ������� �������
	Sprite personSprite;
	float currentFrame;//������� ���� ��������
	int hitPoint;//���������� ������ �������
	std::pair <int,int> sizePerson;//������ � ������ ��������� 
	std::pair <float,float> scaleValue;//��������������� ������� �� x � y
	TileMap *tileMap;//������ �� �����
	void errorMessage(std::string message);//������� ��� ������� � ������ ��������� �� ������ ������� �������� ������
	virtual std::string getNameOfPerson()=0;
};

#endif