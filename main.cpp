#include <SFML/Graphics.hpp>
#include <vector>

#include "Player.h"
#include "Enemy.h"
#include "TileMap.h"
#include "Person.h"
#include "Reward.h"
#include "SpriteHandler.h"
#include "ListHandler.h"

using namespace sf;
int Enemy::numberOfEnemy;
int main()
{
	sf::RenderWindow window(sf::VideoMode(WindowWidth, ground), "Mario", sf::Style::Close);	//�������� �������� ����
	//sf::RenderWindow window(sf::VideoMode(WindowWidth, ground), "Mario");
	TileMap tileMap("Map.csv");//�������� �� ������ ����� csv
	PLAYER myPlayer("hero.png",&tileMap);//�������� ������, �������� ��� ����� ��� ��������� ������� � ������
	std::vector<Person*> enemies;//�������� ������
	srand(time(NULL));
	fillList(enemies,numberOfEnemies,"monster sprite.png",&tileMap,"Enemy");

	std::vector<Person*> coins;//�������� ����� � ���������� ������ ������
	fillList(coins,numberOfEnemies,"coins.png",&tileMap,"Reward");

	std::vector<SpriteHandler*> hitPoints;//������� �������� ������ ����� ������������ � ������ ������� ����
	for (int i = 0; i < myPlayer.getHitPoint(); i++)
	{
		SpriteHandler *heart = new SpriteHandler("HitPoint.png",IntRect(0,0,550,550));
		heart->scaleSprite(0.1);
		hitPoints.push_back(heart);
	}
	Clock clock;//������� ���� ����
	Clock clockProtectionTimeout;//����� � ������� �������� ����� ����� ������
	float protectionTime = clockProtectionTimeout.restart().asMicroseconds();
	float time;
	while (window.isOpen() && myPlayer.getHitPoint()>0 && !coins.empty())//�������� ���� ���� ���� ��� �������� � �� ��� ������ �������
	{
		window.clear(Color::White);
		float time = clock.restart().asMicroseconds();
		time = time/300;//�������� ����
		if (time>200)//������������ ��������� ���������� � �������� �� ����� ����������� ���� 
		{
			time=clock.restart().asMicroseconds()/300;
		}
		

		sf::Event event;
		while (window.pollEvent(event))//��������� ���� ������� ����, ��������� ��������
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}
	
		myPlayer.move();//������� ��������� ������� ������ ���������� ��� ����������� ���������
		if (Keyboard::isKeyPressed(Keyboard::Escape)) { window.close(); }// �������� �������� ���� ���������, ���� ������ ������� Ecscape


		myPlayer.update(time, enemies,&clockProtectionTimeout, coins);//������� ��������� �������������� ��������� � ������� �����. 

		tileMap.drawMap(&window);//��������� �����. ����� �������� ������, ��������� �������� �������� ������ ���


		myPlayer.printHitPoint(hitPoints,&window);//��������� ���������� ������ ������
		
		coins = Person::printPersons(coins,&window,time);//���������� �������
		enemies = Person::printPersons(enemies,&window,time);//���������� ��������
		
		
		window.draw(myPlayer.getPersonSprite());
		window.display();
		time = 0;
		
	}
	//��������� � ����� ����
	window.clear(Color::White);
	tileMap.drawMap(&window);
	SpriteHandler theEnd("Text.png",IntRect(0,140,700,120));
	theEnd.setPosition((WindowWidth-700)/2,(ground-120)/2);
	window.draw(theEnd.getSprite());
	window.display();
	while (window.isOpen() && !(Keyboard::isKeyPressed(Keyboard::Escape)))
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}
	}
	window.close();
	return 0;
}