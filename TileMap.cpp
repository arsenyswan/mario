#include "TileMap.h"
#include <iostream>
TileMap::TileMap(string nameOfFile)
{
	offsetY=0;
	offsetX=0;
	load(nameOfFile);
	getEmptyFields();
}

TileMap::~TileMap()
{
	
	for (int i = 0; i < heightMap; i++)
	{
		delete[] tileMap[i]; 
	}
	delete[] tileMap;
}

void TileMap::load(string nameOfFile)
{
	ifstream in(nameOfFile);
	tileMap = new int*[heightMap];
	for (int i = 0; i < heightMap; i++)
	{
		tileMap[i] = new int [widthMap];
		for (int j = 0; j < widthMap; j++)
		{
			in >> tileMap[i][j];
			in.get();
		}
	}
}
sf::Sprite TileMap::setTextureForMap(string nameOfImages, int left, int top)
{
	mapTexture.loadFromFile(nameOfImages);
	mapSprite.setTexture(mapTexture);
	rect = sf::FloatRect(left, top, fieldSize, fieldSize);
	mapSprite.setTextureRect((sf::IntRect)rect);
	return mapSprite;
}
void TileMap::drawMap(sf::RenderWindow* window)
{
	sf::Sprite mapSprite = setTextureForMap("tileset.png", 0,0);
	//sf::RectangleShape outTile(sf::Vector2f(fieldSize,fieldSize));
	//mapSprite.scale(fieldSize/13,fieldSize/13);//����� ������ ����������� �� �������� �������� 
	for (int i = 0; i < getHeight(); i++)
	{
		for (int j = 0; j < getWidth(); j++)
		{
			
			if (tileMap[i][j]>=groundTile || (tileMap[i][j]>=0 && tileMap[i][j]<=2)) 
			{
				if (tileMap[i][j]==playerTile)
				{
					rect = sf::FloatRect(0, 0, fieldSize, fieldSize);//������� ���������� �������
				}
				else
				{
					rect = sf::FloatRect(0, 0, fieldSize, fieldSize);
				}
				//rect = sf::FloatRect(0, 0, 16, 16);//��������� ������� ����������
				//mapSprite.setTextureRect((sf::IntRect)rect);//outTile.setFillColor(sf::Color::Black);
				
			}
			

			if (tileMap[i][j]==skyTile)
			{
				//rect = sf::FloatRect(256, 0, 16, 16);//�������� ������� ����������

				rect = sf::FloatRect(256, 0, fieldSize, fieldSize);
				
			}
			mapSprite.setTextureRect((sf::IntRect)rect);
			mapSprite.setPosition(j*fieldSize-offsetX,i*fieldSize-offsetY);
			//outTile.setPosition(j*fieldSize,i*fieldSize);		
			//window->draw(outTile);
			window->draw(mapSprite);
		}
	}
}
bool checkFieldsAroundIAndJ(int i,int j, int maxNumOfOccupiedFieldsByPerson, const TileMap &tileMap)//��������������� ������� ��� ���� ����� �����  ��������� �������
{
	int maxI = i+maxNumOfOccupiedFieldsByPerson;
	int maxJ = j+maxNumOfOccupiedFieldsByPerson;
 	for (i; i < maxI; i++)
	{
		for (int k = j; k < maxJ; k++)
		{
			if (!(tileMap.tileMap[i][k]==skyTile))
				return false;
		}
	}
	return true;
}
void TileMap::getEmptyFields() //TODO: ������� ��� ��������� ���� ��������� ����� �� �����, � ������� ����� ����������� �������
{
	int maxPersonSize = 200*0.4; //������������ ������ ������� ��������� c ������ scaleValue
	int maxNumOfOccupiedFieldsByPerson = maxPersonSize/fieldSize;
	for (int i = 2; i < heightMap-maxNumOfOccupiedFieldsByPerson; i++)
	{
		for (int j = 2; j < widthMap-maxNumOfOccupiedFieldsByPerson; j++)
		{
			if (checkFieldsAroundIAndJ(i,j,maxNumOfOccupiedFieldsByPerson, *this))
			{
				emtyFields.push_back(pair<int,int>(i,j));//��� ����� �������� ���������, �� �� ������� � ��������
			}
		}
	}
}
pair<int,int> TileMap::getRandomEmptyField()
{
	int t = rand()%emtyFields.size();
	return emtyFields[t];
}