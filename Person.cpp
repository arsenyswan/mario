#include "Person.h"
//#include "Enemy.h"

Person::Person(String pictureName,  TileMap* const currentMap)
{
	std::cout<<"Person Constructor\n";
	personTecture.loadFromFile(pictureName);
	personSprite.setTexture(personTecture);
	dx=dy=0;
	currentFrame = 0;//������� �������� ��������
	tileMap = currentMap;
	numberOfAnimations=8;
}

Person::~Person()
{
}

int Person:: getHitPoint(){return hitPoint;}
void Person::decreaseHitPoint(){hitPoint--;}

void Person::Collision(dirrection dir)
{
	for (int i = rect.top/fieldSize; (i < (rect.top+rect.height*scaleValue.second)/fieldSize && i < tileMap->getHeight()); i++)
	{
		for (int j = rect.left/fieldSize; (j < (rect.left+rect.width*scaleValue.first)/fieldSize && j < tileMap->getWidth()); j++)
		{	
			if(i>=0 && j>=0)
			{
				if(tileMap->tileMap[i][j]>=80|| (tileMap->tileMap[i][j]>=0 && tileMap->tileMap[i][j]<=2))//160 - ������� ����������� ������ � �����
				{
					if (dir==DirX)
					{
						this->CollisionPersonForX(j);
						//if(dynamic_cast<Enemy*>(this)==NULL) return; // ���� ��� ����, �� �� ������ ������ ����������� ���� ��� � ������ �� ��������� �������� ������������ ���� ����� ������ �������
					}
					if(dy>0 && dir == DirY) //���������� �����������, ������� ��� ��� ��������, ��� ��������� �� ����� � top < ground
					{
						rect.top= i*fieldSize - rect.height*scaleValue.second;
						dy = 0;
						onGround = true;
					}
					if(dy<0 && dir == DirY)
					{
						rect.top = i*fieldSize+fieldSize; //fieldSize;
						dy=0;
					}
				}
			}
			else
			{
				this->errorMessage("Collision function error, coordinate of object is out of range of map, i or j <0");
				rect.top=600;
				rect.left=900;
				break;
			}
		}

	}
}
std::vector<Person*> Person ::printPersons (std::vector<Person*> persons, sf::RenderWindow* window, double time)
{
	for (int i = 0; i < persons.size(); i++)
	{
		if(persons[i]->getHitPoint()>0)
		{
			persons[i]->update(time);
			window->draw(persons[i]->getPersonSprite());
		}
		else
		{
			cout<< persons[i]->getNameOfPerson()<<" objects are left " << persons.size()-1 << endl;
			delete persons[i];
			persons.erase(persons.begin()+i);
			std::vector<Person*> (persons).swap(persons);//���� �� ��������� ����� ��������, �������� ������ �������
			
		}
	}
	return persons;
}
FloatRect Person:: getScaledRect()
{
	FloatRect scaledRect = rect;
	scaledRect.height = scaledRect.height*scaleValue.first;
	scaledRect.width = scaledRect.width*scaleValue.second;
	return scaledRect;
}
void Person::errorMessage(std::string message)
{
	cerr << "object " << this->getNameOfPerson() << " invoke exception: " << message <<endl; 
}