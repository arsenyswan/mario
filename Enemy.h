#ifndef _ENEMY_
#define _ENEMY_
#include "Person.h"
using namespace sf;
class Enemy : public Person
{
public:
	Enemy(String pictureName, TileMap *const currentMap);
	Enemy(String pictureName, TileMap *const currentMap, int positionX, int positionY);
	~Enemy();
	void move();
	void update(double time);
	void decreaseHitPoint();
	float speedOfEnemy;
	static int numberOfEnemy;
private:	
	std::string getNameOfPerson();
	void CollisionPersonForX(int numOfField);
	void initialize(String pictureName, int positionX=600, int positionY=500);

};
#endif


