#include "SpriteHandler.h"

SpriteHandler::SpriteHandler(String FileName)
{
	fileName = FileName;
	texture.loadFromFile(fileName);
	sprite.setTexture(texture);
	rect = IntRect(0,0,100,100);
	sprite.setTextureRect(rect);

}
SpriteHandler::SpriteHandler(String FileName, IntRect iRect) 
{
	//*this = SpriteHandler(FileName);
	fileName = FileName;
	texture.loadFromFile(fileName);
	sprite.setTexture(texture);
	rect = iRect;
	sprite.setTextureRect(rect);
}
void SpriteHandler::mySetTextureRect(IntRect iRect)
{
	rect=iRect;
	sprite.setTextureRect(rect);
}
void SpriteHandler::scaleSprite(float value)
{
	sprite.scale(value,value);
}
void SpriteHandler::setPosition(float x, float y)
{
	sprite.setPosition(x,y);
}
SpriteHandler::~SpriteHandler()
{
}