#include <vector>
#include <string>
#include <iterator>
#include "TileMap.h"

using namespace std;

//template <class T>
void fillList(vector<Person*> &const myVec, int numOfElements, string name, TileMap* tileMap,string type)
{
	for (int i = 0; i < numOfElements; i++)
	{
		Person *newT=nullptr;
		if (type == "Enemy")
		{
			newT = new Enemy(name, tileMap);
		}
		if (type == "Reward")
		{
			newT = new Reward(name, tileMap);
		}
		if (newT!=nullptr)
		{
			myVec.push_back(newT);
		}
		else
		{
			cerr<< "Bad type value for function fillList" << endl;
		}		
	}
}