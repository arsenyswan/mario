#include "Reward.h"


Reward::Reward(String pictureName, TileMap* const currentMap) : Person(pictureName, currentMap)
{
	auto pos = tileMap->getRandomEmptyField();
	scaleValue.first = scaleValueForPerson*2;//0.5;//��� ����������� ��������������� ���������
	scaleValue.second = scaleValueForPerson*2;//��� ����������� ��������������� ���������
	sizePerson.first = 33;//�� �������� ��������
	sizePerson.second = 33;
	rect = FloatRect(0,35,sizePerson.first,sizePerson.second);
	personSprite.setTextureRect((IntRect)rect);
	personSprite.scale(scaleValue.first,scaleValue.second);//��� ����������� ��������������� ���������
	rect.left = pos.second*fieldSize;
	rect.top = pos.first*fieldSize;
	hitPoint = 1;
	//spriteColor = personSprite.getColor();
}

Reward::~Reward()
{
}

void Reward::update(double time)
{
	Collision(DirX);
	if(!onGround) dy=dy+0.0005*time;
	rect.top+=dy*time;
	onGround=false;
	Collision(DirY);
	Collision(DirX);
	personSprite.setTextureRect(IntRect(sizePerson.first*int(currentFrame),33,sizePerson.first,sizePerson.second));
	currentFrame += 0.005*time/2;
	if (currentFrame>numberOfAnimations/2)
	{
		currentFrame-=numberOfAnimations/2;
	}
	personSprite.setPosition(rect.left-this->tileMap->getOffsetX(),rect.top-this->tileMap->getOffsetY());
}
void Reward::move()
{

}

std::string Reward::getNameOfPerson()
{
	return "Reward";
}
void Reward::CollisionPersonForX(int numOfField)
{
	if (dx>0) 
	{
		rect.left = numOfField*fieldSize - rect.width*scaleValue.first;
	}
	if (dx<0)
	{
		rect.left = numOfField*fieldSize + fieldSize;
	}
}