#ifndef _PLAYER_
#define _PLAYER_
#include "Person.h"
#include "Enemy.h"
#include "SpriteHandler.h"
#include "Reward.h"

using namespace sf;
class PLAYER : public Person
{
public:
	PLAYER(String pictureName,TileMap* currentMap);
	~PLAYER();
	void update(double time, vector<Person*> enemies, Clock *protectionTime,vector<Person*> coins);
	void update(double time);
	void move();
	int getHitPoint(){return hitPoint;}
	bool hasProtection;
	void printHitPoint (std::vector<SpriteHandler*> hitPoints, sf::RenderWindow* window);
private:
	std::string getNameOfPerson();
	void goLeft()
	{
		dx=-0.1;
	}
	void goRight()
	{
		dx=0.1;
	}
	void goUp()
	{
		if (onGround)
		{
			dy=-0.5;
			onGround=false;
		}
	}
	void dig();
	void grow();
	Color spriteColor;
	void CollisionPersonForX(int numOfField);
};

#endif
