#include <SFML/Graphics.hpp>
#include <string>
#include <vector>
#include <fstream>

using namespace std;
#ifndef _TILE_MAP_
#define _TILE_MAP_
const int fieldSize = 35;
const int skyTile = 16;
const int groundTile = 80;
const int playerTile=180;

class TileMap
{
private:
	static const int heightMap = 22;
	static const int widthMap = 65;
	sf::Texture mapTexture;
	sf::Sprite mapSprite;
	sf::FloatRect rect;
	float offsetY;
	float offsetX;
	vector<pair<int,int>> emtyFields;
	void getEmptyFields();
	//bool checkFieldsAroundIAndJ(int i,int j, int maxNumOfOccupiedFieldsByPerson);// ��������� � ����������� � TileMap.cpp
public:
	TileMap(string nameOfFile);
	void load(string nameOfFile);
	int **tileMap;
	~TileMap();
	int getHeight(){return heightMap;}
	int getWidth(){return widthMap;}
	void drawMap(sf::RenderWindow*);
	sf::Sprite setTextureForMap(string nameOfImages, int left, int top);
	float getOffsetX(){return offsetX;}
	float getOffsetY(){return offsetY;}
	void setOffsetX(float offset){offsetX=offset;}
	void setOffsetY(float offset){offsetY=offset;}
	static const int indentation = 200;//������ �� ����� ����� ��� ������������� �������� �� � ���������
	pair<int,int> getRandomEmptyField();
};
#endif