#include <SFML/Graphics.hpp>
#include "Person.h"

using namespace sf;
#ifndef _REWARD_
#define _REWARD_


class Reward : public Person
{
public:
	Reward(String pictureName, TileMap* const currentMap);
	~Reward();
	void update(double time);
	void move();
	void CollisionPersonForX(int numOfField);

private:
	std::string getNameOfPerson();

};


#endif