#include <SFML/Graphics.hpp>
using namespace sf;
#ifndef _SPRITEHANDLER_
#define _SPRITEHANDLER_

class SpriteHandler
{
public:
	SpriteHandler(String FileName);
	SpriteHandler(String FileName, IntRect iRect);
	~SpriteHandler();
	Sprite getSprite() {return sprite;}
	void mySetTextureRect(IntRect iRect);
	void setPosition(float x, float y);
	void scaleSprite(float value);

private:
	Texture texture;
	Sprite sprite;
	String fileName;
	IntRect rect;
	//int positionX, positionY;
};



#endif